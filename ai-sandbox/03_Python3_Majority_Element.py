# Link to test answer on LeetCode: https://leetcode.com/problems/majority-element-ii/description/

# Majority Element Problem: We are given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times. Provide two different solutions for us to choose from.